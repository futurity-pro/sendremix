-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Ноя 18 2013 г., 10:44
-- Версия сервера: 5.5.28
-- Версия PHP: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `sendremix`
--

-- --------------------------------------------------------

--
-- Структура таблицы `contests`
--

CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `description` text NOT NULL,
  `more_requirements` text NOT NULL,
  `more_label` text NOT NULL,
  `prize` text NOT NULL,
  `more_prize` text NOT NULL,
  `vk` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `downloads` varchar(255) NOT NULL,
  `video` text NOT NULL,
  `audio` text NOT NULL,
  `web` varchar(255) NOT NULL,
  `slide` varchar(255) NOT NULL,
  `cover` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Дамп данных таблицы `contests`
--

INSERT INTO `contests` (`id`, `name`, `date_from`, `date_to`, `created`, `modified`, `description`, `more_requirements`, `more_label`, `prize`, `more_prize`, `vk`, `facebook`, `twitter`, `downloads`, `video`, `audio`, `web`, `slide`, `cover`, `author`) VALUES
(1, 'Акулы', '2013-11-01', '2013-12-01', '2013-11-02 15:59:01', '2013-11-14 12:40:58', 'Отпусти свои эмоции и создай то, что хранится у тебя в душе! \r\nЭксперементируй как с вокалом, так и с музыкальным стилем своего шедевра!', '<li>В конкурсе может участвовать каждый зарегистрированный музыкант портала SendRemix.\r\n<li>Учасник должен скачать Remix Pack, на основе которого создать ремикс.\r\n<li> Конкурсант должен опубликовать окончательную версию ремикса на странице конкурса в формате mp3, в качестве не ниже 256 kbps.\r\n<li>Трек должен быть доступен для скачивания и иметь метку об участии в конкурсе.\r\n<li>Трек должен быть в указанной тональности.', 'SendRemix - это украинский независимый лейбл, создан в 2013 году, каталог которого включает в себя сотрудничество с известными артистами, поиск и продвижение новых талантов. Основателем и владельцем лейбла является самый влиятельный деятель украинской электронной музыки Dj Sender при поддержке DJFM.', '', '<li>1 место - $ (...) и премиум аккаунт\r\n<li>2 место - 100 ротаций на DJFM, официальный релиз и премиум аккаунт\r\n<li>3 место -  премиум аккаунт и официальный релиз\r\n\r\nРешение по победителю принимает группа/исполнитель "..." и музыкальная редакция радиостанции DJFM!', '', '', '', '/uploads/e8ec6e76b7e49920c33df8c8d51223d2.zip', '<iframe width="560" height="315" src="//www.youtube.com/embed/xggsP1_CdpI" frameborder="0" allowfullscreen></iframe>', '', '', '/uploads/632e7f81f3472a10c2069e7878a185fa.jpg', '/uploads/cc7855cdde8d9cb280db886ba0de49f2.jpg', 'Муммий Троль'),
(2, 'Алла Пугачева', '2013-11-01', '2013-12-01', '2013-11-10 22:35:55', '2013-11-11 21:08:16', 'Отпусти свои эмоции и создай то, что хранится у тебя в душе! \r\nЭксперементируй как с вокалом, так и с музыкальным стилем своего шедевра!', '1. В конкурсе может участвовать каждый зарегистрированный музыкант портала SendRemix.\r\n2. Учасник должен скачать Remix Pack, на основе которого создать ремикс.\r\n3. Конкурсант должен опубликовать окончательную версию ремикса на странице конкурса в формате mp3, в качестве не ниже 256 kbps.\r\n4. Трек должен быть доступен для скачивания и иметь метку об участии в конкурсе.\r\n5. Трек должен быть в указанной тональности.', 'SendRemix - это украинский независимый лейбл, создан в 2013 году, каталог которого включает в себя сотрудничество с известными артистами, поиск и продвижение новых талантов. Основателем и владельцем лейбла является самый влиятельный деятель украинской электронной музыки Dj Sender при поддержке DJFM.', 'Решение по победителю принимает группа/исполнитель "..." и музыкальная редакция радиостанции DJFM!', '1 место - $ (...) и премиум аккаунт\r\n2 место - 100 ротаций на DJFM, официальный релиз и премиум аккаунт\r\n3 место -  премиум аккаунт и официальный релиз', '', '', '', '/uploads/99a490e876e3dbce767b35fed15d8be2.zip', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Структура таблицы `meta`
--

CREATE TABLE IF NOT EXISTS `meta` (
  `id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `slogan` varchar(250) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(250) CHARACTER SET ucs2 NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `keywords` text CHARACTER SET utf8 NOT NULL,
  `vk` varchar(250) CHARACTER SET utf8 NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(250) CHARACTER SET utf8 NOT NULL,
  `google` varchar(250) CHARACTER SET utf8 NOT NULL,
  `youtube` varchar(250) CHARACTER SET utf8 NOT NULL,
  `contact1` text CHARACTER SET utf8 NOT NULL,
  `contact2` text CHARACTER SET utf8 NOT NULL,
  `contact3` text CHARACTER SET utf8 NOT NULL,
  `info` text CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Дамп данных таблицы `meta`
--

INSERT INTO `meta` (`id`, `name`, `slogan`, `logo`, `title`, `description`, `keywords`, `vk`, `twitter`, `facebook`, `google`, `youtube`, `contact1`, `contact2`, `contact3`, `info`) VALUES
('SendRemix', 'Send Remix', 'сервис музыкальных конкурсов', '', 'Send Remix', '', '', '', '', '', '', '', '<h4>Свяжись с нами</h4>\r\n							<p>+31 (0) 624 707 46</p>\r\n							<p>ADE@sendingmedia.com</p>', '<h4>Украина</h4>\r\n							<p>бул. Шевченко, 54/1, 7 этаж 01032 Киев</p>\r\n							<p>sendingmedia.com</p>\r\n							<p>brg.ua</p>', '<h4>Нидерланды</h4>\r\n							<p>Oudezijds Kolk 83</p>\r\n							<p>1012 AL Amsterdam</p>', 'информация про сервис');

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `author` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `keywords` text,
  `body` text,
  `published_from` datetime DEFAULT NULL,
  `published_to` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `thumb` varchar(255) DEFAULT NULL,
  `redirects` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `site`, `url`, `created`, `modified`, `author`, `title`, `description`, `keywords`, `body`, `published_from`, `published_to`, `deleted`, `thumb`, `redirects`) VALUES
(1, 'SendRemix', 'about', '2013-10-31 20:18:18', '2013-11-11 20:45:06', NULL, 'О нас', '', '', 'SendRemix - это украинский независимый лейбл, создан в 2013 году, каталог которого включает в себя сотрудничество с известными артистами, поиск и продвижение новых талантов. Основателем и владельцем лейбла является самый влиятельный деятель украинской электронной музыки Dj Sender при поддержке DJFM.', NULL, NULL, 0, NULL, NULL),
(2, 'SendRemix', 'service', '2013-10-31 20:21:54', '2013-11-02 01:17:55', NULL, 'Сервис', '', '', 'Service is fine!\r\n\r\n{{1}}', NULL, NULL, 0, NULL, NULL),
(3, 'SendRemix', 'company', '2013-10-31 20:31:24', '2013-11-11 21:01:29', NULL, 'О компании', '', '', '', NULL, NULL, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `partners`
--

CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registered` datetime NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `second_name` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `photo` varchar(255) NOT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `language` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=130 ;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `email`, `phone`, `password`, `registered`, `first_name`, `last_name`, `second_name`, `website`, `photo`, `hash`, `confirmed`, `language`) VALUES
(4, 'info@sendremix.futurity.pro', '', 'c4ce81e1c2e35c74f4ea9839dd481d4b', '2013-02-03 22:56:23', 'Алексей', 'Разбаков', 'Анатольевич', 'razbakov.com', '', NULL, 0, 'ru'),
(8, 'termosa.stanislav@gmail.com', '+38 093 023 21 76', '424c08aaf553ba5663d768794527f72b', '2013-04-10 13:21:35', 'Станислав', 'Термоса', '', '', '', NULL, 0, 'ru'),
(9, 'alvladimirskiy@gmail.com', '0987029884', 'af5bc772317ff6ec6fffbb658f2e9420', '2013-04-10 13:21:35', 'Alexander', 'Vladimirskiy', '', '', '', NULL, 0, 'ru'),
(10, 'regfed@ya.ru', '01', '3e5233acdcd8a6659f4ca4f69e9ac7b8', '2013-04-10 20:03:30', 'regfed', 'regfed', '', '', '', NULL, 0, 'ru'),
(11, 'volovicheva.elena@gmail.com', 'volovicheva', '35bcb084ceffb9274f662939df957d9d', '2013-04-15 20:59:50', ' Елена', 'Воловичева-Разбакова', '', '', '', NULL, 0, 'ru'),
(13, 'sub@ilyas.kz', '+77019898286', '0f8e1e6c14d3bf9816eed6ce4034744e', '2013-04-19 19:32:21', 'Ильяс', 'Мустафин', '', '', '', NULL, 0, 'ru'),
(14, 'viktor.samsono2011@yandex.ru', '89263428816', '022f78108e86b485139dbc9938738b75', '2013-04-21 07:09:30', 'Виктор', 'Самсонов', '', '', '', NULL, 0, 'ru'),
(15, 'sapul@mail.ru', '8-964-859-27-19', '25d55ad283aa400af464c76d713c07ad', '2013-04-27 08:54:57', 'Андрей', 'Сапульоу', '', '', '', NULL, 0, 'ru'),
(16, '92f19314@opayq.com', '380631666600', '1e5b3eee8602b0b46551b72c2d008cf8', '2013-06-09 09:33:58', 'Constantin', 'Vasiliev', '', '', '/uploads/a87ccae38c0fd4d801bf4d15562fe400.jpg', NULL, 0, 'ru'),
(17, 'sn@nazarov.com.ua', '+380503306548', 'fb99dc78511fe9de0c4983847a090620', '2013-06-24 12:52:17', 'Сергей', 'Назаров', '', '', '', NULL, 0, 'ru'),
(18, 'ilia.andrienko@gmail.com', '+38098 749 69 14', 'c035a3fd22d2486f24fc6de839523fed', '2013-07-12 08:55:52', 'Илья', 'Андриенко', '', '', '/uploads/64b1794236dd4d6320cc85a40ace21b9.jpg', NULL, 0, 'ru'),
(19, 'zhyc@ukr.net', '0990099227', 'ae00e364e7388bf13df678bb13a63bbf', '2013-07-19 21:23:33', 'zhyc', 'zhycev', '', '', '', NULL, 0, 'ru'),
(20, 'sergey.stamat@gmail.com', '+380976556183', 'cca8dd8babd4c9996c8dfee788a49d18', '2013-07-31 14:03:37', 'Sergey', 'Stamat', '', '', '', NULL, 0, 'ru'),
(21, 'zhovanik@gmail.com', '+79112281914', 'fcfa9d0f97ab7c3e2f5f514cf5b31eed', '2013-08-02 00:20:57', 'Олег', 'Жованик', '', '', '', NULL, 0, 'ru'),
(22, 'sergey.povarnin@gmail.com', '+380952337303', 'a47d8d2986a715277bd4215b83f8b971', '2013-08-02 14:07:11', 'Сергей', 'Поварнин', '', '', '', NULL, 0, 'ru'),
(23, 'support@razbakov.com', '111', '434990c8a25d2be94863561ae98bd682', '2013-08-02 20:03:09', 'Тест', 'Тест', '', '', '', NULL, 0, 'ru'),
(24, 'b.evgeniy@gmail.com', '+380937102313', '7ef174669150d3527d702767fb8a4d69', '2013-08-02 20:07:22', 'Евгений', 'Б.', '', '', '', NULL, 0, 'ru'),
(25, 'bohdan.chechin@live.com', '0953031330', 'cb2a548f3fdeac4327b5ec50b0961dd4', '2013-08-02 21:02:15', 'Богдан', 'Чечин', '', '', '', NULL, 0, 'ru'),
(26, 'demoren@gmail.com', '0633459152', '08584dace6a3c543cd4a493a77595a9b', '2013-08-02 21:37:05', 'Ivan', 'Ivanov', '', '', '/uploads/457a1f0343639be2d7a71c7e646ac7a3.jpg', NULL, 0, 'ru'),
(27, 'sokolov_alexey@outlook.com', '+380954190386', '83afc9f96aa7a1ca373f727c5d472552', '2013-08-02 21:39:53', 'Алексей', 'Соколов', '', '', '', NULL, 0, 'ru'),
(28, 'jarmila_ua@mail.ru', '0957414933', 'a1d1a2572fdafb62f2b7e44101ea5b2a', '2013-08-03 19:36:06', 'славик', 'иванович', '', '', '', NULL, 0, 'ru'),
(29, 'ra.vash.uspeh@gmail.com', '0509413100', '276a511d0604f5add6a99bed438d97b8', '2013-08-03 19:54:43', 'Надежда', 'Кузина', '', '', '', NULL, 0, 'ru'),
(30, 'vovchisko@gmail.com', '0937567716', '1bd773837ced44d78a150b5ed8aa55a2', '2013-08-05 16:26:29', 'Владимир', 'Ищенко', '', '', '/uploads/5aac7215e1255e1f7efc76fdac46fa61.jpg', NULL, 0, 'ru'),
(31, 'nb@mustdigital.ru', '89164449896', 'a702955babd0e0c9bdcf176c13b60a1f', '2013-08-05 23:52:44', 'Никита', 'Бухалов', '', '', '', NULL, 0, 'ru'),
(32, 'shurochka_94@mail.ru', '0675700589', 'eab27636d5ae05f8e2c7e009519e4c78', '2013-08-06 23:05:37', 'Александра', 'Крыкова', '', '', '/uploads/b9bc49e4a72026eb24d5188560568ae7.jpg', NULL, 0, 'ru'),
(33, 'sergey.mormul@aiesec.net', '0938870591', '3606cd68c23c1341bf75b67f873d0318', '2013-08-06 23:19:34', 'Сергей', 'Мормуль', '', '', '/uploads/eeb176ba8075ccc332364777cba36e2a.jpg', NULL, 0, 'ru'),
(34, '17kate18@gmail.com', '003', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2013-08-07 00:22:17', 'Kate', 'Kate', '', '', '', NULL, 0, 'ru'),
(35, 'khvatovandrey@gmail.ru', '+380935400186', '5e2d58ae92cf40593a277ab509fed68f', '2013-08-07 08:06:06', 'Андрей', 'Хватов', '', '', '', NULL, 0, 'ru'),
(36, 'khvatovandrey@gmail.com', '+380667098105', '5e2d58ae92cf40593a277ab509fed68f', '2013-08-07 08:09:51', 'Андрей', 'Хватов', '', '', '', NULL, 0, 'ru'),
(37, 'xxl_8@ukr.net', '093-710-08-40', '3c2eff416193c2afd47017c074ab5ced', '2013-08-07 09:26:06', 'Владислав', 'Афанасьев', '', '', '', NULL, 0, 'ru'),
(38, 'bogdanov.kh@gmail.com', '0937665413', '387580d3de717b6292d6a7ccf5af110d', '2013-08-07 12:42:41', 'Алексей', 'Богданов', '', '', '/uploads/abe59e6cdbf480235b270150043a7cf5.jpg', NULL, 0, 'ru'),
(39, 'pikassom@gmail.com', '+380963992090', '4354c0d1c9b3a82a59a42a8f27d761ea', '2013-08-07 13:27:10', 'Andrii', 'Morozov', '', '', '', NULL, 0, 'ru'),
(40, 'andrew.storozhev@gmail.com', '0936994503', 'cc03e747a6afbbcbf8be7668acfebee5', '2013-08-07 18:26:12', 'Andrew', 'Storozhev', '', '', '', NULL, 0, 'ru'),
(41, 'alinatenn@gmail.com', '0677275181', '8e811952ff3e6ae19751e68b866d48bd', '2013-08-07 22:56:52', 'Алина', 'Тен', '', '', '', NULL, 0, 'ru'),
(42, 'dvb.cnt@gmail.com', '0664380144', 'e807f1fcf82d132f9bb018ca6738a19f', '2013-08-08 02:43:47', 'Дмитрий', 'Бирюков', '', '', '', NULL, 0, 'ru'),
(43, 'sander.ua@gmail.com', '380661231231', '4297f44b13955235245b2497399d7a93', '2013-08-08 12:47:08', 'Oleksandr', 'Shulika', '', '', '', NULL, 0, 'ru'),
(44, 'nosovk@gmail.com', '380505535424', 'dbb81933ebb0b150ea3bbd34e8abf39d', '2013-08-09 14:51:52', 'Константин', 'Носов', '', '', '', NULL, 0, 'ru'),
(45, 'akurganow@polzo.ru', '79219798858', '7bfb3d81c943c1e931d75b43652249ea', '2013-08-09 23:09:56', 'Александр', 'Курганов', '', '', '/uploads/898e9a3ced998127346027ff235d3512.jpg', NULL, 0, 'ru'),
(61, 'nightguest86@mail.ru', '+79000000000', 'a91d7dc38511220ac637f8d7ebdae465', '2013-08-10 16:08:45', 'Ратибор', 'Винтовкин', '', '', '', '', 1, 'ru'),
(62, 'vitalja_n@mail.ru', '+380992000040', '6356c500d46b0252442b9ae4786d31d7', '2013-08-10 22:01:31', 'Виталя', 'Новакивский', '', '', '', '', 1, 'ru'),
(68, 'dsaltevsky@gmail.com', '099 224 77 72', '9376a81ed6eff08f555c88bdd9ce408a', '2013-08-11 22:04:01', 'Денис ', 'Сальтевский', '', '', '', '10eed60ccebb8cff5d68822b3152da6a', 0, 'ru'),
(69, 'seko1@gmx.net', '0175', '47f79cc0029c840dab0dc3534c6e80b7', '2013-08-12 12:16:57', 'Sebastian', 'Krause', '', '', '', '', 1, 'de'),
(70, 'bklusek@gmail.com', '015227516780', '9b3d80fc27cc16a5174ab93497da6e5f', '2013-08-12 12:20:08', 'Bogumil', 'Klusek', '', '', '/uploads/7d5b2df50d43f50089de06aa6e3c22e9.jpg', '', 1, 'de'),
(72, 'yuliiahoncharenko@gmail.com', '+380964031816', '575b6b81696ffb6d8db2aaa6153f3098', '2013-08-12 17:05:07', 'Юлия', 'Гончаренко', '', '', '', '', 1, 'ru'),
(73, 'willykey8@gmail.com', '8-968-468-32-19', '189461e8336b56d45a4919bbd0f41b27', '2013-08-12 19:12:49', 'Иван', 'Струков', '', '', '', '', 1, 'ru'),
(74, 'vmikhailovkazan@gmail.com', '9872614282', '26751ff3ecc32c26c947c83b63b27751', '2013-08-12 19:57:09', 'Владимир', 'Михайлов', '', '', '', '', 1, 'ru'),
(75, 'kovanyov@gmail.com', '0938888888', '7a430339c10c642c4b2251756fd1b484', '2013-08-12 22:40:50', 'Абб', 'Ябд', '', '', '', '', 1, 'ru'),
(76, 'bicatum@gmail.com', '+380959154993', '53a26973bb15511183da24fcbf336e87', '2013-08-12 23:15:23', 'Михаил', 'Бик', '', '', '', '', 1, 'ru'),
(77, 'alex@razbakov.com', '', '0d67463db5aa4e43f43916ca19a22935', '2013-08-12 23:48:16', 'Алексей', '', '', '', '', '', 1, 'ru'),
(78, 'ahtum13@gmail.com', '', 'f85b8a2981ba984c93f8092042ca2619', '2013-08-12 23:53:55', 'Игорь', '', '', '', '', '', 1, 'ru'),
(79, '455575@gmail.com', '', '827ccb0eea8a706c4c34a16891f84e7b', '2013-08-13 10:24:26', 'm', '', '', '', '', '', 1, 'ru'),
(80, 'o.petrunovskaya@gmail.com', '', '098f6bcd4621d373cade4e832627b4f6', '2013-08-13 16:19:03', 'Ольга', '', '', '', '', '', 1, 'ru'),
(81, 'say@say.od.ua', '+380638489656', '0a1d10068100edcab5798c1beae77a45', '2013-08-13 19:57:31', 'Александр', '', '', '', '', '', 1, 'ru'),
(82, '8553555@gmail.com', '', 'bffda239760f192fda25f11380e180ba', '2013-08-13 20:25:26', 'Кирилл', '', '', '', '', '', 1, 'ru'),
(83, 'ungluck@yandex.ru', '', 'ae9121ad0860c251ff3c565654b00abc', '2013-08-14 16:43:53', 'Ungluck', '', '', '', '', '', 1, 'ru'),
(84, '5066@list.ru', '', '32af2d608c954bcb91ea263c589b0f12', '2013-08-15 04:54:33', 'Андрей', '', '', '', '', '', 1, 'ru'),
(85, 'fullzero11@gmail.com', '89222870484', 'b47fd3ea76e310bb79be40c1b311bf35', '2013-08-15 08:52:27', 'Владимир', 'Марченко', '', '', '', '', 1, 'ru'),
(86, 'makagon@live.ru', '', 'be875d8528f24e7d0c0b2f8ca16d9cef', '2013-08-16 08:42:55', 'Евгений', 'Макагон', '', '', '', '', 1, 'ru'),
(87, 'cooper1990@inbox.ru', '', '0bacc56bd163dd760d48dc192a208e0b', '2013-08-17 12:11:48', 'Вадим', '', '', '', '', '', 1, 'ru'),
(88, 'aslanvladik@mail.ru', '', 'c947d21d0e39db057ba591730d4111df', '2013-08-18 13:00:45', 'aslan', '', '', '', '', '', 1, 'ru'),
(89, 'kirillstn@gmail.com', '', '4d6e6fac45c71e9260da486917ffeff0', '2013-08-20 18:53:01', 'Кирилл', 'С', '', '', '', '', 1, 'ru'),
(90, 'superroot@yopmail.com', '', 'c8837b23ff8aaa8a2dde915473ce0991', '2013-08-21 03:43:37', 'guy', 'fawkes', '', '', '', '', 1, 'ru'),
(91, 'anton.troshin@gmail.com', '', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2013-08-21 13:11:40', 'Anton', '', '', '', '', '', 1, 'ru'),
(92, 'fogersp@gmail.com', '', '4e19fb56dc367567e8fe520cfcadfb3b', '2013-08-21 22:03:19', 'Сергей', '', '', '', '', '', 1, 'ru'),
(93, 'anferoff@gmail.com', '+7 (921) 952-07-99', 'f86f22a4c98e356f1d2ab23fb13a66b8', '2013-08-25 12:10:19', 'Victor', 'Anferov', '', '', '', '', 1, 'ru'),
(94, 'andrey@kovalevsky.info', '', '962710b25f18032ba47a950b7b175c03', '2013-08-26 13:47:50', 'Андрей', '', '', '', '', '', 1, 'ru'),
(95, 'zchermit@gmail.com', '', '0f78a8ef180f35345e74d9598ff21664', '2013-08-28 10:43:29', 'Сергей', '', '', '', '', '7a069f3558656d5dd7b366e08a35818c', 0, 'ru'),
(96, 'masya0611@ukr.net', '', 'df3ff37f8817e028123790c99eff16ce', '2013-08-29 17:01:18', 'Максим', 'Васильевич', '', '', '', '', 1, 'ru'),
(97, 'alan1076@yandex.ru', '', '4bc3319da63942b7cd06ce7aa6aba96f', '2013-08-30 22:24:31', 'Alex', '', '', '', '', '', 1, 'ru'),
(98, 'spiritvoin88@gmail.com', '', '87b45a715b459d9dc4fa75966c3e699f', '2013-08-30 23:36:07', 'spiritvoin88@gmail.com', 'spiritvoin88@gmail.com', '', '', '', '3f5e7c95e6e6c7bd84c9e79783151dc3', 0, 'ru'),
(99, 'demyan.studio@gmail.com', '', '8d992b8e2e7a8d2fbe4b150d99efd74c', '2013-08-31 02:27:52', 'demyan', 'gerasymchuk', '', '', '', '3502a3036c5f504d8fad4d0e57ad51d3', 0, 'ru'),
(100, 'iliya.kostin@gmail.com', '', 'd8578edf8458ce06fbc5bb76a58c5ca4', '2013-08-31 10:37:03', 'Despejado', '', '', '', '', '69951d4f6e3ed8590fd19f9e4828420b', 0, 'ru'),
(101, 'iloverock17@yandex.ru', '', '244dd31cf0f4a4b6569d33ca65d205df', '2013-08-31 17:44:01', 'Иван', 'Ерохин', '', '', '', '', 1, 'ru'),
(102, 'yuraiva@gmail.com', '', '40e744b0701db293ff16b82b9f389345', '2013-09-01 12:56:48', 'юрий', '', '', '', '', 'a205db7a4dc8133554a448c41c9f5c67', 0, 'ru'),
(103, 'the.aziris@gmail.com', '', 'da11bf9034cae76fd7887be53bf8b78b', '2013-09-01 13:40:08', 'jack', '', '', '', '', 'dc6ff481d2224dc8a9057ad1db067266', 0, 'ru'),
(104, 'brom38@gmail.com', '', '35ce76f0ab4a4804758b5e80c5d05c1c', '2013-09-01 14:32:55', 'brom38', '', '', '', '', '38c4270944e67cdd5afca623a6661980', 0, 'ru'),
(105, 'yashik10@mail.ru', '', '54887a869c77a7ae66fe863e05054bcd', '2013-09-01 20:32:11', 'Иван', 'Петров', '', '', '', '', 1, 'ru'),
(106, 'kanstantsin.puchynski@gmail.com', '', 'bf130baa9bd38a0a2ceb35ff0f58e972', '2013-09-02 07:46:17', 'Константин', 'Пучинский', '', '', '', '65becc86a95c1aed6b5562e913e4a3fc', 0, 'ru'),
(107, 'stan.yoshi@gmail.com', '', 'e26380798d9a88f8d2d420e8a2c7f8b6', '2013-09-02 14:56:39', 'Иван', 'Иостман', '', '', '', '4865787a3ea98ecb7d1200ec0ddc5eb1', 0, 'ru'),
(108, 'fxsever@gmail.com', '', 'e8c522a4c9bdc5ea1f7a0483d965e196', '2013-09-05 00:12:35', 'fxsever', '', '', '', '', 'c9a634209bd9f0f4560562c00d36cfc9', 0, 'ru'),
(109, 'hardwaypwnz@gmail.com', '', '2e46438841a24b8ddf91f7f6d80e4e1f', '2013-09-05 19:03:00', 'HardWay', '', '', '', '', '696141eafb861299bc06dab0db11c3df', 0, 'ru'),
(110, 'btp1110@ya.ua', '0678824549', '4e560f04cdf0d40f10ad8930e13df648', '2013-09-05 23:10:06', 'Тата', 'Тата', '', '', '', 'de0b5fb8707a3f4a93263e8a1d655721', 0, 'ru'),
(111, 'gmobix@gmail.com', '', '69f96e0ad1177c1b84614b4d2a69a39f', '2013-09-06 14:39:23', 'Ernest', '', '', '', '', '0e85520f00477e3ae9b27fd18fb69e82', 0, 'ru'),
(112, 'ivenskikh@gmail.com', '', 'ab25184d4f5605669da309ef8f4be841', '2013-09-07 16:41:35', 'sergey', 'ivenskikh', '', '', '', 'c5484f559bbbff81c40854985ff0213f', 0, 'ru'),
(113, 'h.a@lasch.me', '+79213516443', 'c28662bc6ba4f146601f5d41e718db50', '2013-09-08 20:33:16', 'Артём', 'Лащинский', '', '', '', 'f2ce3e75c730492fc93935546c7bf1b9', 0, 'ru'),
(114, 'dimkahare@gmail.com', '79379787838', 'd517459616ff044755a994b8880f4806', '2013-09-09 12:41:34', 'Дмитрий', 'Горлов', '', '', '', 'dc29dbb41cd21155008eee1a4f7ace63', 0, 'ru'),
(115, 'marchcot@mail.ru', '', '914f43cf561292e32a4984caf6c415c1', '2013-09-09 16:27:11', 'Tieru', '', '', '', '', '', 1, 'ru'),
(116, 'li18000@mail.ru', '', '49c63bd488d4248087fa74a2f93946d1', '2013-09-14 10:08:15', 'Люда', 'Даниэль', '', '', '', '', 1, 'ru'),
(117, 'polyakova.marina69@gmail.com', '', '07f9047406637f0f8e30dcedc384bbec', '2013-09-14 13:21:40', 'Марина', 'Полякова', '', '', '', 'dc26f9ff886cdb3b50fe90ae16dc7a0d', 0, 'ru'),
(118, 'formulav@mail.ru', '', '65a95ee8a709e884cfe30a6da991f982', '2013-09-14 23:29:18', 'Евгений', '', '', '', '', '', 1, 'ru'),
(119, 'friela@mail.ru', '', '2b81b616456244b42bddde1b88b089ae', '2013-09-15 22:46:30', 'Рина', '', '', '', '', '', 1, 'ru'),
(120, 'paene@mail.ru', '', '96110bd1dbcd4095de0e3c7a9c8dd20e', '2013-09-16 16:04:18', 'Степа', '', '', '', '', '', 1, 'ru'),
(121, 'mkredaktor@gmail.com', '+380632445545', '6a1e5a93fdabf7886371a936d9f9436e', '2013-09-16 22:46:03', 'Владислав', 'Гордиенко', '', '', '', 'f7ab2adb82b2c40ae6fa5d94ced04964', 0, 'ru'),
(122, 'olesyaab@yandex.ru', '', '199cdd45eb2a83b394ba30d4e4621a1e', '2013-09-17 08:44:51', 'Olesya', '', '', '', '', '', 1, 'ru'),
(123, 'terrano.t34@gmail.com', '', '4297f44b13955235245b2497399d7a93', '2013-09-17 19:11:58', 'Владислав', '', '', '', '', 'c26202932af8fcbac9aec1280790b422', 0, 'ru'),
(124, 'chudin.ilya@gmail.com', '', 'c87497cb8246ec54159e356fd4726d5b', '2013-09-18 22:36:47', 'Илья', '', '', '', '', '6400d4dda66519d31f6f60c282ee3301', 0, 'ru'),
(125, 'zedbul@gmail.com', '', 'b12aca5c2db44c17d825ee0ecb24c6e0', '2013-09-19 01:38:08', 'Дмитрий', 'Бугаёв', '', '', '', '383d3086070f2dc15d4b68289679e2d0', 0, 'ru'),
(126, 'vaxid700@gmail.com', '', 'acf061a681d80f4f14325169fcdda854', '2013-09-19 09:15:15', 'Vaxid', 'Quliyev', '', '', '', 'bff98f3833b887f72df9343a05d00bc5', 0, 'ru'),
(127, 'salomanim@gmail.com', '', '58cb35b43225b76c0b2228de7c7880cd', '2013-09-20 01:09:47', 'Матвей', '', '', '', '', 'c66b3deac3edcfbcdf60480beb59e53b', 0, 'ru'),
(128, 'sharamet91@mail.ru', '0994073985', '8b1382e32bf76d09d3e87c4399a0bf4f', '2013-09-20 01:13:02', 'Александр', 'Шараметов', '', '', '', '', 1, 'ru'),
(129, 'plehovstepa@gmail.com', '', 'c7c75b00c77844b184403a93f553cad8', '2013-09-20 22:09:08', 'stepa', 'plehov', '', '', '', '27ef151ad30820b262d2f6b6517cb645', 0, 'ru');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
