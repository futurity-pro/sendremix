<div class="row lp">
    <div class="span12">

        <div class="center">
            <h2>Управление личными финансами</h2>
            <p class="big grey">Всё на одной странице</p>

            <!-- Image -->

            <div class="lp-image">
                <img src="<?php echo Yii::app()->baseUrl; ?>/images/finance.png">
            </div>

            <!-- Button -->

            <div class="button">
                <a href="<?php echo $this->createUrl('/cabinet/accounts/register') ?>">Попробовать</a>
            </div>

            <div class="border"></div>

        </div>

        <div class="row">
            <div class="span6">

                <h4>Счета</h4>
                <p>Хочешь знать сколько было на твоих счетах месяц назад? Ты можешь знать состояние своих счетов в любой день прошлого или будущего</p>

                <hr>

                <h4>Бюджет</h4>
                <p>Забываешь о важных проплатах? Бюджет на год позволяет разграничить суммы на разные нужды и контролировать их каждый день, не давая случайным покупкам растранжирить все деньги</p>

                <hr>
            </div>

            <div class="span6">

                <h4>Журнал</h4>
                <p>Прошла неделя, а зарплаты и след простыл? Ежедневное ведение журнала расходов и доходов позволит отслеживать движения личных средств и всегда быть в курсе когда и сколько было потрачено</p>

                <hr>

                <h4>Корректировки</h4>
                <p>Нет возможности вести журнал ежедневно? Функция <strong>Сверить счета</strong> позволяет внести корректировки в систему учета так, чтобы данные оставались актуальными</p>

                <hr>
            </div>

        </div>

    </div>
    <div class="span12" style="margin-bottom: 20px">
        <h2>Отзывы</h2>
        <script type="text/javascript">
            $(function(){
                $('.hover-block a').click(function(e){
                    e.preventDefault();
                })
            })
        </script>
        <ul class="hover-block">

            <li>

                <a href="#">
                    <!-- Image -->
                    <img src="/images/kurganov.jpg" alt="">
                    <!-- Content with background color Class -->
                    <div class="hover-content b-orange" style="top: 125px;">
                        <h6>Александр Курганов</h6>
                        <small>разработчик в Amplifr, 27 лет</small>
                        Мне идея нравится и уже выглядит хорошо, чем смогу помогу
                    </div>
                </a>

            </li>


            <li>

                <a href="#">
                    <img src="/images/sokolov.jpg" alt="">
                    <div class="hover-content b-red" style="top: 125px;">
                        <h6>Алексей Соколов</h6>
                        <small>сеошник, 25 лет</small>
                        Не то чтобы "все-говно", наборот - идея хрошо, но тут вагон вопросов по интерфейсу
                    </div>
                </a>

            </li>


            <li>

                <a href="#">
                    <img src="/images/ten.jpg" alt="">
                    <div class="hover-content b-purple" style="top: 125px;">
                        <h6>Алина Тен</h6>
                        <small>студентка, 22 года</small>
                        Привет) сегодня совсем немного попробовала твой проект)
                        Идея - крута, мне самой пригодится)
                    </div>
                </a>

            </li>


            <li>

                <a href="#">
                    <img src="/images/krikova.jpg" alt="">
                    <div class="hover-content b-lblue" style="top: 125px;">
                        <h6>Александра Крыкова</h6>
                        <small>студентка, 22 года</small>
                        лично я ничего подобного пока не видела.
                        программа очень полезная, думаю многие захотят ею воспользоваться
                    </div>
                </a>

            </li>

        </ul>
    </div>
</div>

<aside class="span3">
    <!-- VK Widget -->
    <div id="vk_groups"></div>
    <script type="text/javascript">
        VK.Widgets.Group("vk_groups", {mode: 0, width: "220", height: "400", color1: 'FFFFFF', color2: '2B587A', color3: '5B7FA6'}, 39214962);
    </script>
</aside>
<aside class="span4">
    <div class="fb-like-box" data-href="https://www.facebook.com/cabinetio" data-width="292" data-height="400" data-show-faces="true" data-header="false" data-stream="false" data-show-border="true"></div>
</aside>
<aside class="span4">
    <a class="twitter-timeline" href="https://twitter.com/search?q=%23cabinet_ru" data-widget-id="366253192708755456">Твиты о "#cabinet_ru"</a>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
</aside>