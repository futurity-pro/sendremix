<div class="modal fade" id="modal_02" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<?php $register_form = $this->beginWidget('CActiveForm', array(
		'id'=>'register-form',
		'action'=> $this->createUrl('/accounts/register'),
		'enableAjaxValidation' => TRUE,
		'method' => 'POST',
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
			'validateOnChange'=>true,
			'validateOnType'=>false,
		),
	)); ?>
	<div class="modal-dialog modal-registration">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div class="modal-body">
				<div class="email-enter clearfix">
					<label>
						<?php echo $register_form->textField($user,'first_name', array('placeholder'=>'Имя')); ?>
						<?php echo $register_form->error($user,'first_name'); ?>
					</label>
					<label>
						<?php echo $register_form->textField($user,'second_name', array('placeholder'=>'Фамилия')); ?>
						<?php echo $register_form->error($user,'second_name'); ?>
					</label>
					<label>
						<?php echo $register_form->textField($user,'phone', array('placeholder'=>'Телефон')); ?>
						<?php echo $register_form->error($user,'phone'); ?>
					</label>
					<label>
						<?php echo $register_form->textField($user,'email', array('placeholder'=>'Email')); ?>
						<?php echo $register_form->error($user,'email'); ?>
					</label>
					<label>
						<?php echo $register_form->passwordField($user,'password', array('placeholder'=>'Пароль')); ?>
						<?php echo $register_form->error($user,'password'); ?>
					</label>
					<label>
						<?php echo $register_form->passwordField($user,'passwordRepeat', array('placeholder'=>'Повторите пароль')); ?>
						<?php echo $register_form->error($user,'passwordRepeat'); ?>
					</label>

				</div>
				<label class="rememb-password" for="rules">
					<?php echo $register_form->HiddenField($user, 'acceptRules' );?>
					<input type="checkbox" id="rules">
					Я принимаю условия <a href="/rules">Пользовательского соглашения</a>
					<?php echo $register_form->error($user,'acceptRules'); ?>
				</label>
				<script>
					$(function(){
						$("#rules").click(function(e){
							if($(this).val() == "1"){
								$(this).val("0");
								$("#User_acceptRules").val("0");
							}else{
								$(this).val("1");
								$("#User_acceptRules").val("1");
							}
						});
					})
				</script>
			</div>
			<div class="modal-footer">
				<button type="submit"class="btn enter-btn" type="submit">Отправить<i></i></button>
			</div>
		</div>
	</div>
	<?php $this->endWidget(); ?>
</div>