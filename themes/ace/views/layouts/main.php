<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">

    <title><?php echo CHtml::encode($this->pageTitle); ?></title>

    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">

    <meta name="description" content="overview &amp; stats"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>

    <?php Yii::app()->bootstrap->register(); ?>

    <!--basic styles-->

    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/bootstrap-responsive.min.css" rel="stylesheet"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/font-awesome.min.css"/>

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/font-awesome-ie7.min.css"/>
    <![endif]-->

    <!--page specific plugin styles-->

    <!--fonts-->

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-fonts.css"/>

    <!--ace styles-->
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/datepicker.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-responsive.min.css"/>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-skins.min.css"/>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/ace-ie.min.css"/>
    <![endif]-->

    <link rel="stylesheet" href="<?php echo Yii::app()->theme->baseUrl; ?>/assets/css/styles.css"/>
    <!--inline styles related to this page-->
    <style>
        .navbar .navbar-inner {
            background: #438eb9;
        }
    </style>
</head>

<body>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a href="/" class="brand">
                <strong><?php echo Yii::app()->name ?></strong> <?=t('Administrator') ?>
            </a><!--/.brand-->

            <ul class="nav ace-nav pull-right">
                <li class="light-blue">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <?php if(Yii::app()->user->photo): ?>
                        <img class="nav-user-photo" src="<?php echo Yii::app()->user->photo ?>">
                        <?php else: ?>
                        <img class="nav-user-photo" src="/images/nophoto.png">
                        <?php endif;?>
								<span class="user-info">
									<small><?php echo Yii::app()->user->firstName ?></small>
                                    <?php echo Yii::app()->user->lastName ?>
								</span>

                        <i class="icon-caret-down"></i>
                    </a>

                    <?php $this->widget('bootstrap.widgets.TbDropdown', array(
                        'htmlOptions' => array(
                            'class' => 'user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-closer',
                        ),
                        'items' => array(
                            array(
                                'label' => t('Profile'),
                                'icon' => 'user',
                                'url' => array('/accounts/profile')
                            ),
                            '-',
                            array(
                                'label' => t('Logout'),
                                'icon' => 'off',
                                'url' => array('/accounts/logout')
                            ),
                        )
                    )); ?>
                </li>
            </ul>
            <!--/.ace-nav-->
        </div>
        <!--/.container-fluid-->
    </div>
    <!--/.navbar-inner-->
</div>

<div class="main-container container-fluid">
<a class="menu-toggler" id="menu-toggler" href="#">
    <span class="menu-text"></span>
</a>

<div class="sidebar" id="sidebar">
    <?php $this->widget('cabinet-widgets.Nav', array(
    'type'=>'list',
    'items' => array(
        array(
            'label' => t('Content'),
            'icon' => 'sitemap',
            'url' => array('/site/meta'),
            'items' => array(
                array(
                    'label' => t('Meta'),
                    'icon' => 'bookmark',
                    'url' => array('/site/meta')
                ),
                array(
                    'label' => t('Pages'),
                    'icon' => 'globe',
                    'url' => array('/site/pages')
                ),
                array(
                    'label' => t('Contests'),
                    'icon' => 'icon-trophy',
                    'url' => array('/contest/admin')
                ),
				array(
                    'label' => t('members'),
                    'icon' => 'group',
                    'url' => array('/contest/members')
                ),
				array(
                    'label' => t('Users'),
                    'icon' => 'icon-user',
                    'url' => array('/user/admin')
                ),


            )
        ),
        array(
            'label' => t('PR'),
            'icon' => 'male',
            'url' => array('/'),
            'items' => array(
//                array(
//                    'label' => t('Context Ads'),
//                    'icon' => 'lightbulb',
//                    'url' => array('/')
//                ),
                array(
                    'label' => t('Partners'),
                    'icon' => 'beer',
                    'url' => array('/partner/admin')
                ),
//                array(
//                    'label' => t('Banners'),
//                    'icon' => 'bullhorn',
//                    'url' => array('/')
//                ),
            )
        ),

//        array(
//            'label' => t('Support'),
//            'icon' => 'question',
//            'url' => array('/'),
//            'items' => array(
//                array(
//                    'label' => t('Help'),
//                    'icon' => 'info-sign',
//                    'url' => array('/')
//                ),
//                array(
//                    'label' => t('Submit Proposal'),
//                    'icon' => 'envelope',
//                    'url' => array('/')
//                ),
//            )
//        ),
    )
    )); ?>

    <div class="copyright">
        <?php echo t('Control Panel') ?>
        <a target="_blank" href="http://cms.futurity.pro/">FuturityCMS</a>
    </div>
</div>

<div class="main-content">
<div class="breadcrumbs" id="breadcrumbs">
    <?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
        'htmlOptions' => array('class'=>''),
        'homeLink' => CHtml::link(t('Control Panel'), Yii::app()->homeUrl),
        'links' => $this->breadcrumbs,
        'separator' => '<i class="icon-angle-right arrow-icon"></i>'
    )); ?>

    <div class="nav-search" id="nav-search">
    </div>
</div>

<div class="page-content">
<!--/.page-header-->

<div class="row-fluid">
    <?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'error'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'warning'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'info'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
            'danger'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); ?>
    <?php echo $content; ?>

    <div class="clear"></div>
</div>
</div>
<!--/#ace-settings-container-->
</div>
<!--/.main-content-->
</div>
<!--/.main-container-->

<!--basic scripts-->


<!--page specific plugin scripts-->

<!--[if lte IE 8]>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/excanvas.min.js"></script>
<![endif]-->
<script type="text/javascript">
    if("ontouchend" in document) document.write("<script src='<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
</script>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery-ui-1.10.3.custom.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.slimscroll.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.easy-pie-chart.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.sparkline.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/flot/jquery.flot.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/flot/jquery.flot.pie.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/flot/jquery.flot.resize.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/date-time/bootstrap-datepicker.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/moment.min.js"></script>

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/markdown/markdown.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/markdown/bootstrap-markdown.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/jquery.hotkeys.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootstrap-wysiwyg.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/bootbox.min.js"></script>

<!--ace scripts-->

<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/ace-elements.min.js"></script>
<script src="<?php echo Yii::app()->theme->baseUrl; ?>/assets/js/ace.min.js"></script>

<!-- /site/pages -->



<script type="text/javascript">
    $(function(){

        function showErrorAlert (reason, detail) {
            var msg='';
            if (reason==='unsupported-file-type') { msg = "Unsupported format " +detail; }
            else {
                console.log("error uploading file", reason, detail);
            }
            $('<div class="alert"> <button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong>File upload error</strong> '+msg+' </div>').prependTo('#alerts');
        }

        $('#editor').ace_wysiwyg();
    });
</script>
<!-- -->

<script type="text/javascript">
    $(function () {
        $('.submenu .active').parent().parent().addClass('active open');
    })
</script>

</body>
</html>