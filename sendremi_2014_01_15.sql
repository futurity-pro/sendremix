-- 
-- Structure for table `contests`
-- 
SET FOREIGN_KEY_CHECKS=0;

DROP TABLE IF EXISTS `contests`;
CREATE TABLE IF NOT EXISTS `contests` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `description` text NOT NULL,
  `description_under` text,
  `more_requirements` text NOT NULL,
  `more_label` text NOT NULL,
  `prize` text NOT NULL,
  `more_prize` text NOT NULL,
  `vk` varchar(255) NOT NULL,
  `facebook` varchar(255) NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `downloads` text,
  `video` text NOT NULL,
  `audio` text NOT NULL,
  `web` varchar(255) NOT NULL,
  `slide` varchar(255) DEFAULT NULL,
  `cover` varchar(255) DEFAULT NULL,
  `author` varchar(255) NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- 
-- Data for table `contests`
-- 

INSERT INTO `contests` (`id`, `name`, `date_from`, `date_to`, `created`, `modified`, `description`, `description_under`, `more_requirements`, `more_label`, `prize`, `more_prize`, `vk`, `facebook`, `twitter`, `downloads`, `video`, `audio`, `web`, `slide`, `cover`, `author`, `title`) VALUES
  ('1', 'The HardKiss \"Shadows of time\"', '2013-01-02', '2013-01-10', '2013-11-02 15:59:01', '2014-01-13 11:26:16', '', '<p>&nbsp;</p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Ты ощущаешь в себе талант?&nbsp;</span></p>\r\n<p><br /><span style=\"font-family: arial,helvetica,sans-serif;\">Хочешь сделать классный ремикс на экслюзивный ремикспак от группы <strong>The HARDKISS</strong>? </span><br /><br /><span style=\"font-family: arial,helvetica,sans-serif;\">Тогда воспользуйся шансом и участвуй в конкурсе ремиксов при поддержке <strong>DJFM и Send Remix</strong>! </span><br /><br /><span style=\"font-family: arial,helvetica,sans-serif;\">Покажи новое звучание этому треку и получи статус официального ремиксера! </span><br /><br /><span style=\"font-family: arial,helvetica,sans-serif;\">Трек <strong>\"Shadows Of Time\"</strong> - заглавный саундтрек к фильму Любомира Левицкого \"Тени незабытых предков\". </span><br /><br /><span style=\"font-family: arial,helvetica,sans-serif;\">На создание песни музыкантов вдохновила мистическая история, разворачивающаяся в украинском триллере\" </span><br /><span style=\"font-family: arial,helvetica,sans-serif;\">Что же вдохновит тебя? </span><br /><br /><span style=\"font-family: arial,helvetica,sans-serif;\"><strong>Лучшие ремиксы попадут в релиз и получат ротацию на радиостанции DJ FM!</strong></span></p>', '<p>&bull; В конкурсе может принять участие любой музыкант, зарегистрированный на портале <strong>Send Remix</strong>!<br /><br />&bull; Конкурсный ремикс должен соответствовать творческому заданию и быть предоставлен в формате mp3, в качестве не ниже 256 kbps!</p>\r\n<p>&bull; Тональность трека: \"Соль Минор\"</p>\r\n<p>&bull; Сэмплы и другой материал, используемый в ремиксе, не должны нарушать авторских прав или прав третьих лиц!<br /><br />&bull; Права на все ремиксы принадлежат правообладателю в лице <strong>&laquo;Send Records&raquo; (как правообладателю оригинала)</strong>, любое коммерческое использование ремиксов, без предварительного письменного согласования,  запрещено!<br /><br />&bull; Некоммерческое использование ремиксов позволено в рамках личных.<br />социальных сетей. Ремикс может быть опубликован на личных страницах <strong>Myspace, Soundcloud, Vkontakte</strong> и других социальных сетях в качестве, не выше <strong>MP3 128kbps</strong> и не длиннее 3 минут. Любая возможность скачивания должна быть запрещена. Ремикс может быть использован в клубных выступлениях и музыкальных подкастах.<br /><br />&bull; Победитель конкурса имеет право на призовой фонд только при условии подписания дополнительного контракта c <strong>&ldquo;Send Records&rdquo;</strong> и предоставлении окончательно версии ремикса в качестве<strong> WAV/44100/Stereo/16 bit.<br /><br /><br /></strong></p>', '<p>Send Remix - это украинская независимая организация, созданная в 2013 году, целью которой есть сотрудничество с артистами, создание ремикс моделей, поиск и продвижение современного звучания и талантливых артистов. Мы открыты к сотрудничеству и целенаправленному движению!</p>', '<p>Официальный релиз и ротации на DJ FM.</p>', '<p>1. Официальный релиз<br /><br />2. Ротации на DJ FM<br /><br />3. Интервью и гос<span>тевой микс в</span> радиошоу \"Bomba\"&nbsp; с DJ Sender.</p>', 'http://vk.com/the_hardkiss_band', 'http://www.facebook.com/THEHARDKISS', 'https://twitter.com/THE_HARDKISS', 'http://sendremix.com/uploads/remixpack/THE%20HARDKISS%20-%20Shadows%20of%20time%20(Remix%20Pack%20130%20bpm)%20.zip', '<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/aNfWSWf5LvI?rel=0\" frameborder=\"0\" allowfullscreen></iframe>', '<iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/111374987&amp;color=ff6600&amp;=true&amp;show_artwork=false\"></iframe>', 'http://www.thehardkiss.com/', '/uploads/7e3a504e52dd63ecd4588af4d18e7c83.jpg', '/uploads/191c57859033e0b34d56ac0bf7e754bd.jpg', '', 'О конкурсе'),
  ('3', 'Воплi Вiдоплясова \"Щедрик\"', '2013-12-25', '2014-02-01', '2013-12-09 15:14:30', '2014-01-13 16:37:03', '', '<p><span style=\"font-family: arial, helvetica, sans-serif;\">\"Вопли Видоплясова\" - самая роковая группа Украины, объявляет конкурс на лучший ремикс песни \"Щедрик\"!</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Щедрик - рождественская украинская народная песня, получившая всемирную популярность!</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Создай и ты новогоднее настроение!&nbsp;</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Эксперементируй со стилем и голосом!</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Задай новый ритм этой зимы и стань официальным ремиксером группы \"ВВ\"!&nbsp;</span></p>\r\n<p><span style=\"font-family: arial, helvetica, sans-serif;\">Держи ритм вместе с нами!</span></p>\r\n<p>&nbsp;</p>\r\n<p>&nbsp;</p>\r\n<h1 class=\"example1\" style=\"text-align: justify;\"></h1>', '<p>&bull; В конкурсе может принять участие любой музыкант, зарегистрированный на портале&nbsp;<strong>Send Remix</strong>!<br /><br />&bull; Конкурсный ремикс должен соответствовать творческому заданию и быть предоставлен в формате mp3, в качестве не ниже 256 kbps!</p>\r\n<p>&bull; Тональность трека: \"Ля Минор\"</p>\r\n<p>&bull; Сэмплы и другой материал, используемый в ремиксе, не должны нарушать авторских прав или прав третьих лиц!<br /><br />&bull; Права на все ремиксы принадлежат правообладателю в лице&nbsp;<strong>&laquo;Send Records&raquo; (как правообладателю оригинала)</strong>, любое коммерческое использование ремиксов, без предварительного письменного согласования, запрещено!<br /><br />&bull; Некоммерческое использование ремиксов позволено в рамках личных.<br />социальных сетей. Ремикс может быть опубликован на личных страницах&nbsp;<strong>Myspace, Soundcloud, Vkontakte</strong>&nbsp;и других социальных сетях в качестве, не выше&nbsp;<strong>MP3 128kbps</strong>&nbsp;и не длиннее 3 минут. Любая возможность скачивания должна быть запрещена. Ремикс может быть использован в клубных выступлениях и музыкальных подкастах.<br /><br />&bull; Победитель конкурса имеет право на призовой фонд только при условии подписания дополнительного контракта c&nbsp;<strong>&ldquo;Send Records&rdquo;</strong>&nbsp;и предоставлении окончательно версии ремикса в качестве<strong>&nbsp;WAV/44100/Stereo/16 bit.<br /><br /></strong></p>', '<p>Send Remix - это украинская независимая организация, созданная в 2013 году, целью которой есть сотрудничество с артистами, создание ремикс моделей, поиск и продвижение современного звучания и талантливых артистов. Мы открыты к сотрудничеству и целенаправленному движению!</p>', '<p>Официальный релиз и ротации на DJ FM.</p>', '<p>1. Официальный релиз<br /><br />2. Ротации на DJ FM<br /><br />3. Интервью и гостевой микс в&nbsp;радиошоу \"Bomba\"&nbsp; с DJ Sender.</p>', 'http://vk.com/voplividopliassova', 'https://www.facebook.com/vopli', '', 'http://sendremix.com/uploads/remixpack/VV%20Shchedryk%20185%20AUDIO%2048-24.zip', '<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/IKBreuAv1TE?rel=0\" frameborder=\"0\" allowfullscreen></iframe>', '<iframe src=\"http://promodj.com/embed/4528726/big\" width=\"100%\" height=\"70\" style=\"min-width: 1200px; max-width: 2000px\" frameborder=\"0\" allowfullscreen></iframe>', 'https://www.vopli.com.ua', '/uploads/efd47fd7f9fbc8188b7161f0cce89e14.jpg', '/uploads/34bc6a445ae2e4118006799760d9d6a8.jpg', '', 'О конкурсе'),
  ('5', 'SENDER \"SUNLIGHT\"', '2013-01-13', '2013-01-15', '2014-01-13 11:14:18', '2014-01-13 16:16:55', '', '<p>ghcjvjvjf</p>', '<p>&bull; В конкурсе может принять участие любой музыкант, зарегистрированный на портале&nbsp;<strong>Send Remix</strong>!<br /><br />&bull; Конкурсный ремикс должен соответствовать творческому заданию и быть предоставлен в формате mp3, в качестве не ниже 256 kbps!</p>\r\n<p>&bull; Тональность трека: \"Ре Минор\"</p>\r\n<p>&bull; Сэмплы и другой материал, используемый в ремиксе, не должны нарушать авторских прав или прав третьих лиц!<br /><br />&bull; Права на все ремиксы принадлежат правообладателю в лице&nbsp;<strong>&laquo;Send Records&raquo; (как правообладателю оригинала)</strong>, любое коммерческое использование ремиксов, без предварительного письменного согласования, запрещено!<br /><br />&bull; Некоммерческое использование ремиксов позволено в рамках личных.<br />социальных сетей. Ремикс может быть опубликован на личных страницах&nbsp;<strong>Myspace, Soundcloud, Vkontakte</strong>&nbsp;и других социальных сетях в качестве, не выше&nbsp;<strong>MP3 128kbps</strong>&nbsp;и не длиннее 3 минут. Любая возможность скачивания должна быть запрещена. Ремикс может быть использован в клубных выступлениях и музыкальных подкастах.<br /><br />&bull; Победитель конкурса имеет право на призовой фонд только при условии подписания дополнительного контракта c&nbsp;<strong>&ldquo;Send Records&rdquo;</strong>&nbsp;и предоставлении окончательно версии ремикса в качестве<strong>&nbsp;WAV/44100/Stereo/16 bit.<br /><br /></strong></p>', '<p>Send Remix - это украинская независимая организация, созданная в 2013 году, целью которой есть сотрудничество с артистами, создание ремикс моделей, поиск и продвижение современного звучания и талантливых артистов. Мы открыты к сотрудничеству и целенаправленному движению!</p>', '<p>Официальный релиз и ротации на DJFM</p>', '<p>1. Официальный релиз<br /><br />2. Ротации на DJ FM<br /><br />3. Интервью и гостевой микс в&nbsp;радиошоу \"Bomba\"&nbsp; с DJ Sender.</p>', 'http://vk.com/sender', 'http://www.facebook.com/eu.sender', 'https://twitter.com/senderdj', 'http://sendremix.com/uploads/remixpack/sunlight_remixpack.rar', '<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/G0a2ehIYkog\" frameborder=\"0\" allowfullscreen></iframe>', '<iframe width=\"100%\" height=\"166\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/114296818&amp;color=ff6600&amp;auto_play=false&amp;show_artwork=true\"></iframe>', 'http://eugenesender.com', '/uploads/51ccab5b23249c8655e5496b098091e8.jpg', '/uploads/a2be026a905151de3797aadeba3305f6.jpg', '', 'О конкурсе');

-- 
-- Structure for table `dowloads_stat`
-- 

DROP TABLE IF EXISTS `dowloads_stat`;
CREATE TABLE IF NOT EXISTS `dowloads_stat` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `contest_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- 
-- Structure for table `meta`
-- 

DROP TABLE IF EXISTS `meta`;
CREATE TABLE IF NOT EXISTS `meta` (
  `id` varchar(50) CHARACTER SET utf8 NOT NULL,
  `name` varchar(250) CHARACTER SET utf8 NOT NULL,
  `slogan` varchar(250) CHARACTER SET utf8 NOT NULL,
  `logo` varchar(250) CHARACTER SET ucs2 NOT NULL,
  `title` varchar(250) CHARACTER SET utf8 NOT NULL,
  `title_prizes` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title_requirements` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `title_about` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `keywords` text CHARACTER SET utf8 NOT NULL,
  `vk` varchar(250) CHARACTER SET utf8 NOT NULL,
  `twitter` varchar(255) NOT NULL,
  `facebook` varchar(250) CHARACTER SET utf8 NOT NULL,
  `google` varchar(250) CHARACTER SET utf8 NOT NULL,
  `youtube` varchar(250) CHARACTER SET utf8 NOT NULL,
  `contact1` text CHARACTER SET utf8 NOT NULL,
  `contact2` text CHARACTER SET utf8 NOT NULL,
  `contact3` text CHARACTER SET utf8 NOT NULL,
  `info` text CHARACTER SET utf8 NOT NULL,
  `facebook_client_id` varchar(100) NOT NULL,
  `facebook_client_secret` varchar(100) NOT NULL,
  `vkontakte_client_id` varchar(100) NOT NULL,
  `vkontakte_client_secret` varchar(100) NOT NULL,
  `soundcloud_client_id` varchar(100) NOT NULL,
  `soundcloud_client_secret` varchar(100) NOT NULL,
  `soundcloud_return_url` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 
-- Data for table `meta`
-- 

INSERT INTO `meta` (`id`, `name`, `slogan`, `logo`, `title`, `title_prizes`, `title_requirements`, `title_about`, `description`, `keywords`, `vk`, `twitter`, `facebook`, `google`, `youtube`, `contact1`, `contact2`, `contact3`, `info`, `facebook_client_id`, `facebook_client_secret`, `vkontakte_client_id`, `vkontakte_client_secret`, `soundcloud_client_id`, `soundcloud_client_secret`, `soundcloud_return_url`) VALUES
  ('SendRemix', 'Send Remix', 'сервис музыкальных конкурсов', '/uploads/6636d801b9d5edaa3141125bb18f32f9.png', 'Send Remix', 'Призы', 'Условия участия', 'о Лейбле', '', '', 'http://vk.com/sendremix', 'https://twitter.com/Sendremix', 'https://www.facebook.com/sendremix', '', 'https://www.youtube.com/SendRemix', '<h4>Свяжись с нами</h4>\r\n<p>+38 (050) 340 20 80</p>\r\n<p>tarik@sendingmedia.com</p>', '', '<h4>Украина</h4>\r\n<p>бул. Шевченко, 54/1, 7 этаж 01032 Киев</p>\r\n<p>sendingmedia.com</p>\r\n<p>brg.ua</p>', '<p>Мы создали Send Remix как платформу, направленную на развитие культуры  электронной музыки. Наше стремление - это обеспечение успешного  сотрудничества артистов различных музыкальных жанров. <br /><br />Здесь каждый может  проявить свой талант и мастерство продюсера, открыть новые творческие  горизонты при поддержке признанных звезд, профессионального менеджмента и целевых медиа. <br /><br />Участие и победа в конкурсах от Send Remix - это новый этап  развития карьеры артистов всех стилей и направлений.</p>', '263761137107823', '62e55a4605945a95dd895a635f2677f7', '4007300', 'Se1fGd7xVABC02wjMpwV', 'dc5450a1358aca6e1052faf9768e377e', '0d32d6e004dd06d30ea277876069256e', 'http://sendremix.com/accounts/soundcloud');

-- 
-- Structure for table `pages`
-- 

DROP TABLE IF EXISTS `pages`;
CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `site` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `author` int(11) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `keywords` text,
  `body` text,
  `published_from` datetime DEFAULT NULL,
  `published_to` datetime DEFAULT NULL,
  `deleted` tinyint(4) NOT NULL DEFAULT '0',
  `thumb` varchar(255) DEFAULT NULL,
  `redirects` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- 
-- Data for table `pages`
-- 

INSERT INTO `pages` (`id`, `site`, `url`, `created`, `modified`, `author`, `title`, `description`, `keywords`, `body`, `published_from`, `published_to`, `deleted`, `thumb`, `redirects`) VALUES
  ('1', 'SendRemix', 'about', '2013-10-31 20:18:18', '2013-12-02 18:41:11', NULL, 'О нас', '', '', '<p>Send Remix - это украинский независимая организация, созданная в 2013 году, целью которой есть сотрудничество с артистами, создание ремикс моделей, поиск и продвижение современного звучания и талантливых артистов. Мы открыты к сотрудничеству и целенаправленному движению!</p>', NULL, NULL, '0', NULL, NULL),
  ('2', 'SendRemix', 'service', '2013-10-31 20:21:54', '2013-11-02 01:17:55', NULL, 'Сервис', '', '', 'Service is fine!\r\n\r\n{{1}}', NULL, NULL, '0', NULL, NULL),
  ('3', 'SendRemix', 'company', '2013-10-31 20:31:24', '2013-11-11 21:01:29', NULL, 'О компании', '', '', '', NULL, NULL, '0', NULL, NULL),
  ('4', 'SendRemix', 'rules', '2013-12-02 18:19:42', '2013-12-02 18:19:42', NULL, 'Правила', '', '', '<p>&nbsp;\"Мы еще работаем над этим разделом\"</p>', NULL, NULL, '0', NULL, NULL);

-- 
-- Structure for table `partners`
-- 

DROP TABLE IF EXISTS `partners`;
CREATE TABLE IF NOT EXISTS `partners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `link` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- 
-- Data for table `partners`
-- 

INSERT INTO `partners` (`id`, `name`, `link`, `image`) VALUES
  ('2', 'DJ FM', 'http://djfm.ua', '/uploads/43935b7ba76e68b9430b928f20d5ec0b.jpg'),
  ('3', 'Geometria', 'http://geometria.ru/kiev', '/uploads/1250726f572c70d236b20cc47e833af8.jpg'),
  ('5', 'BRG', 'http://brg.ua/', '/uploads/86cc148c8b6a679239dfa6a7152ed356.jpg'),
  ('6', 'SMG', 'http://sendingmedia.com/ru/', '/uploads/16773ad509f7a33ef225db60d4abcbc9.jpg');

-- 
-- Structure for table `remix`
-- 

DROP TABLE IF EXISTS `remix`;
CREATE TABLE IF NOT EXISTS `remix` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `contest_id` int(11) NOT NULL,
  `original_file_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `remix_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- 
-- Data for table `remix`
-- 

INSERT INTO `remix` (`id`, `user_id`, `file_name`, `created_on`, `contest_id`, `original_file_name`) VALUES
  ('28', '1', 'b66614e7f4adcf7791d456149e652bba.mp3', '2013-12-05 16:42:13', '1', 'Andrew_York_-_Three_Dances_III._Gigue'),
  ('29', '1', '7a5f2d73f330414c3c8a86df5da580d5.mp3', '2013-12-09 18:46:29', '1', 'Polynomial C - Maceo Plex edit');

-- 
-- Structure for table `users`
-- 

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `registered` datetime NOT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `second_name` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `hash` varchar(255) DEFAULT NULL,
  `confirmed` tinyint(4) DEFAULT '0',
  `language` varchar(2) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `vk_id` bigint(20) DEFAULT NULL COMMENT 'vkontakte',
  `fb_id` bigint(20) DEFAULT NULL COMMENT 'facebook',
  `sc_id` bigint(20) DEFAULT NULL COMMENT 'suncloud',
  `role` smallint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

-- 
-- Data for table `users`
-- 

INSERT INTO `users` (`id`, `email`, `phone`, `password`, `registered`, `first_name`, `last_name`, `second_name`, `website`, `photo`, `hash`, `confirmed`, `language`, `city`, `country`, `vk_id`, `fb_id`, `sc_id`, `role`) VALUES
  ('1', 'tark@sendingmedia.com', '8-546-454-624', '3b739eaf42d6cf5623c2dc08ce10c1a9', '0000-00-00 00:00:00', 'Taras', 'Yur', 'DAS', '', '/uploads/b66614e7f4adcf7791d456149e652bba.mp3', NULL, '0', NULL, 'Киев', 'Россия', NULL, NULL, NULL, '2'),
  ('4', 'info@sendremix.futurity.pro', '8-546-454-624', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-04 17:28:15', 'Send', 'Rimix', 'Анатольевич', 'https://www.facebook.com/send.rimix', '/uploads/efbb96fb9bcea54a9d367e68e7716e93.mp3', NULL, '0', NULL, 'Москва', 'Россия', NULL, '100007143841025', NULL, '1'),
  ('5', 'admin', '', '3b739eaf42d6cf5623c2dc08ce10c1a9', '2013-12-05 10:40:51', 'Admin', '', '', '', '', NULL, '0', NULL, '', '', NULL, NULL, NULL, '2'),
  ('7', 'kattie@ukr.net', '', 'af61c5cdda6ec939788e9eb4ce89a7a4', '2013-12-07 15:04:11', 'KatrinLeoPako', '', '', '', '', '240571d6a383ef94833690ab89b88c9a', '0', 'ru', '', '', NULL, NULL, NULL, '1'),
  ('8', 'tarik@sendingmedia.com', 'ВФЫЫФВ', '670b14728ad9902aecba32e22fa4f6bd', '2013-12-09 18:49:40', 'Тарас', '', 'вдыфдвыф', '', '', 'e2496252c830df568b5baf8da2f29de3', '0', 'ru', '', '', NULL, NULL, NULL, '1'),
  ('9', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-09 18:58:21', 'Саркисова', 'Каринка', NULL, 'http://vkontakte.ru/id1345489', '', NULL, '0', NULL, '', '', '1345489', NULL, NULL, '1'),
  ('10', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-09 19:01:39', 'Юрченко', 'Тарас', NULL, 'http://vkontakte.ru/id9217442', '', NULL, '0', NULL, '', '', '9217442', NULL, NULL, '1'),
  ('11', 'sarkisova.k8@gmail.com', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-11 19:06:13', 'Карина', 'Саркисова', NULL, 'https://www.facebook.com/karina.sarkisova', '', NULL, '0', NULL, '', '', NULL, '100000634756095', NULL, '1'),
  ('12', 'tarik@sendingmedia.com', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-11 19:53:51', 'Taras', 'Yurchenko', NULL, 'https://www.facebook.com/taras.yurchenko', '', NULL, '0', NULL, '', '', NULL, '100000228273571', NULL, '1'),
  ('13', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-21 21:45:19', 'Чайковская', 'Любаша', NULL, 'http://vkontakte.ru/id21927946', '', NULL, '0', NULL, '', '', '21927946', NULL, NULL, '1'),
  ('14', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2013-12-23 10:41:05', 'Богданов', 'Андрей', NULL, 'http://vkontakte.ru/id179317570', '', NULL, '0', NULL, '', '', '179317570', NULL, NULL, '1'),
  ('17', 'dimu493@mail.ru', '+393283384055', '6cb34ca5bc15f3e4c72cb6f20f382224', '2013-12-31 15:55:05', 'Dmytro', '', 'Kutsyi', '', '', 'd2e8ff9aebc5ea264218ec84634777da', '0', 'ru', '', '', NULL, NULL, NULL, '1'),
  ('18', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2014-01-06 16:10:48', 'Omelchenko', 'Ksenia', NULL, 'http://vkontakte.ru/id125664795', '', NULL, '0', NULL, '', '', '125664795', NULL, NULL, '1'),
  ('19', 'skyper.d@icloud.com', '+38(093)8602678', 'd0d8c720b8f6b7a05584715954c9e79a', '2014-01-10 02:50:14', 'Олег', '', 'Беспалов', '', '', '3978ab9f1e6e3f2fd3bc2f2ffce67d8b', '0', 'ru', '', '', NULL, NULL, NULL, '1'),
  ('20', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2014-01-10 02:53:06', '', '', NULL, 'https://api.soundcloud.com/users/46283335', 'https://i1.sndcdn.com/avatars-000051110173-23zlbd-large.jpg?d53bf9f', NULL, '0', NULL, '', '', NULL, NULL, '46283335', '1'),
  ('21', '', '', 'd41d8cd98f00b204e9800998ecf8427e', '2014-01-12 16:46:25', 'Романенко', 'Евгений', NULL, 'http://vkontakte.ru/id66234285', '', NULL, '0', NULL, '', '', '66234285', NULL, NULL, '1');

SET FOREIGN_KEY_CHECKS=1;