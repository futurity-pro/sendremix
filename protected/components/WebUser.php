<?php
class WebUser extends CWebUser
{
    private $_model;

    public function getPhoto() {
		$user = $this->getModel();
        return $user->photo;
    }


	public function getFirstName() {
		$user = $this->getModel();
        return $user->first_name;
    }

    public function getLastName() {
		$user = $this->getModel();
        return $user->last_name;
    }

	public function getFullName() {
        $user = $this->getModel();
        return $user->last_name . ' ' . $user->first_name;
    }

	public function getRole() {
		if($user = $this->getModel()){
			return $user->role;
		}
	}

	private function getModel(){
		if (!$this->isGuest && $this->_model === null){
			$this->_model = User::model()->findByPk($this->id);
		}
		return $this->_model;
	}
}
