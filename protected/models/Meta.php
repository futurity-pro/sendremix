<?php

/**
 * This is the model class for table "meta".
 *
 * The followings are the available columns in table 'meta':
 * @property string $id
 * @property string $name
 * @property string $slogan
 * @property string $logo
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $vk
 * @property string $facebook
 * @property string $google
 * @property string $youtube
 * @property string $contact1
 * @property string $contact2
 * @property string $contact3
 * @property string $info
 */
class Meta extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Meta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'meta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id, name', 'required'),
			array('id', 'length', 'max'=>50),
			array('name, slogan, logo, title, vk, facebook, google, youtube, twitter', 'length', 'max'=>250),

			array('facebook_client_id, facebook_client_secret', 'length', 'max' => 100),
			array('vkontakte_client_id, vkontakte_client_secret', 'length', 'max' => 100),
			array('soundcloud_client_id, soundcloud_client_secret ,soundcloud_return_url', 'length', 'max' => 100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array(
				'id, name, slogan, logo, title, description, keywords, vk, facebook, google, youtube, contact1,
				contact2, contact3, info, title_prizes, title_requirements, title_about',
				'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => t('Name'),
			'slogan' => t('Slogan'),
			'logo' => t('Logo'),
			'cover' => t('Cover'),
			'title' => t('Title'),
			'description' => t('Description'),
			'keywords' => t('Keywords'),
			'vk' => t('Vk'),
			'facebook' => t('Facebook'),
			'google' => t('Google'),
			'youtube' => t('Youtube'),
			'contact1' => t('Contact1'),
			'contact2' => t('Contact2'),
			'contact3' => t('Contact3'),
			'info' => t('Info'),

			'title_requirements' => t('Title Requirements'),
			'title_about' => t('Title About Label'),
			'title_prizes' => t('Title Prizes'),

		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('slogan',$this->slogan,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('keywords',$this->keywords,true);
		$criteria->compare('vk',$this->vk,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('google',$this->google,true);
		$criteria->compare('youtube',$this->youtube,true);
		$criteria->compare('contact1',$this->contact1,true);
		$criteria->compare('contact2',$this->contact2,true);
		$criteria->compare('contact3',$this->contact3,true);
		$criteria->compare('info',$this->info,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public static function getMetaModel()
	{
		$meta = Meta::model()->find('id="'.Yii::app()->name.'"');
		return (!empty($meta)) ?  $meta : false;
	}
}