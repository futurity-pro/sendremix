<?php

class User extends CActiveRecord
{
	/* Scenarios */

	const SIGNUP     = 'signup';
	const PROFILE    = 'profile';
	const PASSWORD   = 'password';
	const SEND_REMIX = 'send_remix';
	const CREATE     = 'user_create';

	/* Roles */
	const ROlE_GUEST  = 0;
	const ROlE_ADMIN  = 2;
	const ROLE_USER   = 1;

	public static $dir = '/uploads/';
	public $passwordRepeat;

	public $acceptRules;
	public $contest_id;
	public $file;

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function tableName()
	{
		return 'users';
	}


	public function rules()
	{
        return array(
			array(
				'email, password, passwordRepeat',
				'required',
				'on' => self::CREATE
			),
            array(
                ' first_name, email, password, passwordRepeat',
                //'acceptRules, first_name, email, , passwordRepeat',
                'required',
                'on' => self::SIGNUP,
                'message' => t('Fill {attribute}')
            ),
            array(
                'first_name, email',
                'required',
                'on' => self::PROFILE,
                'message' => t('Fill {attribute}')
            ),

			array('first_name, last_name, email, phone, website, photo, city, country', 'filter', 'filter'=>array($obj=new CHtmlPurifier(),'purify')),

  			array(
                'first_name, second_name, last_name, email, phone,  city, country, file',
                'required',
                'on' => self::SEND_REMIX,
                'message' => t('Fill {attribute}')
            ),
			array(
				'acceptRules',
				'required',
				'requiredValue' => 1,
				'on' => array(self::SEND_REMIX,  self::SIGNUP),
				'message' => t('You must be accept rules')
			),
            //array('email, password', 'required', 'on' => self::LOGIN, 'message' => t('Fill {attribute}')),

            array('email, phone', 'unique', 'on' => array(self::SIGNUP, self::CREATE), 'message' => t('This {attribute} is already registered')),
            array(
                'email, password',
                'length',
                'max' => 255,
                'on' => self::SIGNUP,
                'message' => t('Max length of {attribute} is 255')
            ),
            array(
                'phone',
                'length',
                'max' => 20,
                'on' => self::SIGNUP,
                'message' => t('{attribute} should be max 20 digits')
            ),
            array(
                'password',
                'compare',
                'compareAttribute' => 'passwordRepeat',
                'on' => self::SIGNUP,
                'message' => t('Passwords does not match')
            ),
            array('id, email, phone, password, first_name, last_name, second_name, country, city, vkontakte, facebook, twitter, google, instagram, languages, skills, birth_date, nickname, language', 'safe'),
            array(
                'email',
                'filter',
                'filter' => 'mb_strtolower',
                'on' => self::SIGNUP,
                'message' => t('{attribute} should be in lower case')
            ),
            array('email', 'email', 'on' => self::SIGNUP),
        );
	}


	public function relations()
	{
		return array(
			'remixes' => array(self::HAS_MANY, 'Remix', 'user_id')
		);
	}


	public function attributeLabels()
	{
		return array(
            'id' 				=> 'Id',
            'first_name' 		=> t('First Name'),
            'second_name' 		=> t('Second Name'),
            'last_name' 		=> t('Family Name'),
            'birth_date' 		=> t('Birth Date'),
            'nickname' 			=> t('Nick Name'),
            'email' 			=> t('Email'),
            'phone' 			=> t('Phone'),
            'website' 			=> t('Web Site'),
            'password'			=> t('Password'),
            'passwordRepeat' 	=> t('Repeat Password'),
            'photo' 			=> t('Photo'),
            'country' 			=> t('Country of residence'),
            'city' 				=> t('City of residence'),
            'vkontakte' 		=> t('Vkontakte'),
            'facebook' 			=> t('Facebook'),
            'twitter' 			=> t('Twitter'),
            'google' 			=> t('Google+'),
            'instagram' 		=> t('Instagram'),
            'languages' 		=> t('Languages'),
            'accept_rules' 		=> t('Accept Rules'),
            'role' 		        => t('Role'),
		);
	}


	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);

		$criteria->compare('last_name',$this->first_name,true);
		$criteria->compare('first_name',$this->last_name,true);
		$criteria->compare('email',$this->email,true);

		$criteria->compare('phone',$this->phone,true);

		$criteria->compare('password',$this->password,true);

		return new CActiveDataProvider('User', array(
			'criteria'=>$criteria,
		));
	}

	protected function beforeSave()
	{
		if(parent::beforeSave())
		{
			if($this->email == 'admin'){
				$this->addError('email', 'you can\'t delete or change admin user');
				return false;
			}

			if($this->isNewRecord)
			{
				$this->registered = date('Y-m-d H:i:s');
                $this->password = self::hashPassword($this->password);
			}
			
			if($this->scenario == self::PASSWORD)
			{
				$this->password = self::hashPassword($this->password);
			}
	
			return true;
		}
	
		return false;
	}





	static function hashPassword($password)
	{
		return md5($password);
	}
	
	public function login()
	{
		$identity = new UserIdentity($this->email, $this->password);
		if($identity->authenticate()) {
			Yii::app()->user->login($identity);
			return true;
		} else {
			$this->addError('email', t('Incorrect username or password.'));
			return false;
		}
	}

	public function getPhoto()
	{
		if (!empty($this->photo)){
			return  $this->photo;
		}
		else {
			return Yii::app()->theme->baseUrl  . '/images/no_image.gif';
		}
	}
	public static function getRolesList()
	{
		return array(
			 self::ROLE_USER  => t('User')  ,
			 self::ROlE_ADMIN => t('Admin') ,
		);
	}
}