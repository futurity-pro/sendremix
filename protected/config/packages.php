<?php
return array(

	'main'=> array(
		'baseUrl'=> '/themes/sendremix/',
		'css'=>array(
			'css/bootstrap.css',
			'css/custom.css',
			'css/mystyles.css',
		),
		'js'=>array(
			'js/bootstrap.js',
			'js/custom.js',
		),
		'depends'=>array('jquery')
	),

	'tiny-mce'=> array(
		'baseUrl'=> '/themes/sendremix/',
		'js'=>array(
			'js/tiny_mce/tiny_mce.js',
			'js/init-tiny-mce.js',
		),
		'depends'=>array('jquery')
	),

	'scroll-to'=> array(
		'baseUrl'=> '/themes/sendremix/',
		'js'=>array(
			'js/tiny_mce/tiny_mce.js',
			'js/jquery.scrollTo.js'
		),
		'depends'=>array('jquery')
	),



);
