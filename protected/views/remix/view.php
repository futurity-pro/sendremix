<?php
/* @var $this RemixController */
/* @var $model Remix */

$this->breadcrumbs=array(
	'Remixes'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Remix', 'url'=>array('index')),
	array('label'=>'Create Remix', 'url'=>array('create')),
	array('label'=>'Update Remix', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Remix', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Remix', 'url'=>array('admin')),
);
?>

<h1>View Remix #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'file_name',
	),
)); ?>
