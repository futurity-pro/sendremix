<?php
/* @var $this RemixController */
/* @var $model Remix */

$this->breadcrumbs=array(
	'Remixes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Remix', 'url'=>array('index')),
	array('label'=>'Manage Remix', 'url'=>array('admin')),
);
?>

<h1>Create Remix</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>