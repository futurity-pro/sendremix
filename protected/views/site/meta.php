<?php Yii::app()->clientScript->registerPackage('tiny-mce'); ?>

<h1><?=t('Meta')?></h1>
<div class="span10 widget-container-span ui-sortable">
    <div class="form">
        <?php echo CHtml::beginForm(); ?>
        <?php echo CHtml::errorSummary($model, null, null, array('class'=>'alert')); ?>


        <div class="row site-logo" style="font-size: 20px;">
            <?php if($model->logo): ?>
                <img id="photo" src="<?php echo $model->logo ?>">
            <?php else: ?>
                <?php echo Yii::app()->name ?>
            <?php endif;?>
        </div>
        <br>

        <div class="row">
            <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons'=>array(
                    array(
                        'label' => t('Change logo'),
                        'icon'=>'picture',
                        'htmlOptions'=>array(
                            'data-toggle'=>'modal',
                            'data-target'=>'#uploadPhoto',
                        ),
                    ),
                    array('items'=>array(
                        array('label' => t('Remove logo'), 'url'=>array('/site/removeLogo')),
                    )),
                ),
            )); ?>
            <br><br>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'name'); ?>
            <?php echo CHtml::activeTextField($model,'name'); ?>
        </div>

		<div class="row">
            <?php echo CHtml::activeLabel($model,'title_prizes'); ?>
            <?php echo CHtml::activeTextField($model,'title_prizes'); ?>
        </div>


        <div class="row">
            <?php echo CHtml::activeLabel($model,'title_requirements'); ?>
            <?php echo CHtml::activeTextField($model,'title_requirements'); ?>
        </div>

		 <div class="row">
            <?php echo CHtml::activeLabel($model,'title_about'); ?>
            <?php echo CHtml::activeTextField($model,'title_about'); ?>
        </div>



        <div class="row">
            <?php echo CHtml::activeLabel($model,'title'); ?>
            <?php echo CHtml::activeTextField($model,'title'); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'description'); ?>
            <?php echo CHtml::activeTextArea($model,'description', array('class'=>'mceNoEditor')); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'keywords'); ?>
            <?php echo CHtml::activeTextArea($model,'keywords', array('class'=>'mceNoEditor', 'rows'=>6)); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'contact1'); ?>
            <?php echo CHtml::activeTextArea($model,'contact1', array('class'=>'', 'rows'=>6)); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'contact2'); ?>
            <?php echo CHtml::activeTextArea($model,'contact2', array('class'=>'', 'rows'=>6)); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'contact3'); ?>
            <?php echo CHtml::activeTextArea($model,'contact3', array('class'=>'', 'rows'=>6)); ?>
        </div>

        <div class="row">
            <?php echo CHtml::activeLabel($model,'info'); ?>
            <?php echo CHtml::activeTextArea($model,'info', array('class'=>'', 'rows'=>6)); ?>
        </div>

        <div class="row">
            <h2><?=t('Social Networks')?></h2>
            <?php echo CHtml::activeLabel($model,'vk'); ?>
            <?php echo CHtml::activeTextField($model,'vk'); ?>

 			<?php echo CHtml::activeLabel($model,'twitter'); ?>
            <?php echo CHtml::activeTextField($model,'twitter'); ?>

            <?php echo CHtml::activeLabel($model,'facebook'); ?>
            <?php echo CHtml::activeTextField($model,'facebook'); ?>

            <?php echo CHtml::activeLabel($model,'google'); ?>
            <?php echo CHtml::activeTextField($model,'google'); ?>

            <?php echo CHtml::activeLabel($model,'youtube'); ?>
            <?php echo CHtml::activeTextField($model,'youtube'); ?>
        </div>


        <div class="row">
            <h2><?=t('Social Networks Api')?></h2>
			<h3><?=t('Facebook')?></h3>
            <?php echo CHtml::activeLabel($model,'facebook_client_id'); ?>
            <?php echo CHtml::activeTextField($model,'facebook_client_id'); ?>

			<?php echo CHtml::activeLabel($model,'facebook_client_secret'); ?>
			<?php echo CHtml::activeTextField($model,'facebook_client_secret'); ?>

			<h3><?=t('Vkontakte')?></h3>

			<?php echo CHtml::activeLabel(    $model, 'vkontakte_client_id'); ?>
            <?php echo CHtml::activeTextField($model,'vkontakte_client_id'); ?>

			<?php echo CHtml::activeLabel(    $model, 'vkontakte_client_secret'); ?>
			<?php echo CHtml::activeTextField($model,'vkontakte_client_secret'); ?>

			<h3><?=t('Soundcloud')?></h3>
			<?php echo CHtml::activeLabel($model,'soundcloud_client_id'); ?>
            <?php echo CHtml::activeTextField($model,'soundcloud_client_id'); ?>

			<?php echo CHtml::activeLabel($model,'soundcloud_client_secret'); ?>
            <?php echo CHtml::activeTextField($model,'soundcloud_client_secret'); ?>


			<?php echo CHtml::activeLabel($model,'soundcloud_return_url'); ?>
            <?php echo CHtml::activeTextField($model,'soundcloud_return_url'); ?>
        </div>

        <br><br>
        <?php echo CHtml::submitButton(t('Save'), array('class'=>'btn btn-primary')); ?>

        <?php echo CHtml::endForm(); ?>
    </div><!-- form -->

    <?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadPhoto')); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?=t('Upload Logo')?></h4>
    </div>

    <div class="modal-body">
        <?php
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFile',
                'config' => array(
                    'action' => Yii::app()->createUrl('/site/UploadLogo'),
                    'allowedExtensions' => array("jpg", "jpeg", "gif", "png"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
					$('#photo').attr('src', '/uploads/'+responseJSON.filename);
					$.post('".Yii::app()->createUrl('/site/ChangeLogo')."', {filename: '/uploads/' + responseJSON.filename});
				}"
                )
            ));
        ?>
    </div>

    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => t('Close'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss'=>'modal'),
        )); ?>
    </div>

    <?php $this->endWidget(); ?>
</div>