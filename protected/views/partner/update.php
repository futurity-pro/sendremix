<?php
$this->breadcrumbs = array(
    t('PR') => array('admin'),
	t('Partners') => array('admin'),
	$model->name
);
?>

<h1><?=t('Update Partner')?> <?php echo $model->name; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>