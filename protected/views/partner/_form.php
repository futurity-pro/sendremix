<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'partner-form',
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->textFieldRow($model,'name',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'link',array('class'=>'span5','maxlength'=>255)); ?>

    <div class="row">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'buttons'=>array(
                array(
                    'label' => t('Change image'),
                    'icon'=>'picture',
                    'htmlOptions'=>array(
                        'data-toggle'=>'modal',
                        'data-target'=>'#uploadImage',
                    ),
                ),
                array('items'=>array(
                    array('label' => t('Remove image'), 'url'=>array('/partner/removeImage?id='.$model->id)),
                )),
            ),
        )); ?>
        <br><br>
    </div>

    <div class="row partner-image">
        <?php if($model->image): ?>
            <img id="photo" src="<?php echo $model->image ?>">
        <?php else: ?>
            <?=t('No image')?>
        <?php endif;?>
    </div>
    <br><br>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? t('Create') : t('Save'),
		)); ?>
	</div>

<?php $this->endWidget(); ?>

<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'uploadImage')); ?>

    <div class="modal-header">
        <a class="close" data-dismiss="modal">&times;</a>
        <h4><?=t('Upload Image')?></h4>
    </div>

    <div class="modal-body">
        <?php
        $this->widget('ext.EAjaxUpload.EAjaxUpload',
            array(
                'id' => 'uploadFile',
                'config' => array(
                    'action' => Yii::app()->createUrl('/partner/upload'),
                    'allowedExtensions' => array("jpg", "jpeg", "gif", "png"),
                    'sizeLimit' => 10*1024*1024,
                    'minSizeLimit' => 1,
                    'onComplete'=>"js:function(id, fileName, responseJSON){
					$('#photo').attr('src', '/uploads/'+responseJSON.filename);
					$.post('".Yii::app()->createUrl('/partner/changeImage')."?id=".$model->id."', {filename: '/uploads/' + responseJSON.filename});
				}"
                )
            ));
        ?>
    </div>

    <div class="modal-footer">
        <?php $this->widget('bootstrap.widgets.TbButton', array(
            'label' => t('Close'),
            'url' => '#',
            'htmlOptions' => array('data-dismiss'=>'modal'),
        )); ?>
    </div>

<?php $this->endWidget(); ?>