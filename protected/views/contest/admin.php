<?php
$this->breadcrumbs=array(
    t('Content')=>array('/site/meta'),
	t('Contests'),
);
?>
<h1><?php echo t('Manage Contests')?></h1>


<?php echo CHtml::link(t('Create Contest'), array('/contest/create'), array('class'=>'btn btn-primary')); ?>
<br/>
<br/>
<span class="label label-warning arrowed arrowed-right">Общая статистика</span>
<br/>
<br/>
<div class="infobox infobox-grey infobox-small infobox-dark">
	<div class="infobox-icon">
		<i class="icon-download-alt"></i>
	</div>

	<div class="infobox-data">
		<div class="infobox-content">Загрузки</div>
		<div class="infobox-content"> <?php echo $download_stat?></div>
	</div>
</div>

<div class="infobox infobox-green infobox-small infobox-dark">
	<div class="infobox-icon">
		<i class="icon-download-alt"></i>
	</div>

	<div class="infobox-data">
		<div class="infobox-content">Ремиксы</div>
		<div class="infobox-content"><?php echo $remixes_stat?></div>
	</div>
</div>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'contest-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'date_from',
		'date_to',

		'contest'=>array(
			'value' => "'<img src=\"/assets/admin/images/move.png\" />'",
			'header' => t('Contest'),
			'value'  => '$data->getStat()',

		),

		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
