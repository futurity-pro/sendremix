<?php
Yii::app()->clientScript->registerPackage('tiny-mce');
$this->breadcrumbs=array(
    t('Content')=>array('/site/meta'),
	t('Contests') => array('admin'),
	$model->name
);
?>

<h1><?php echo t('Update Contest'); echo ' '.$model->name; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>