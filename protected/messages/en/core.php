<?php

return array (
    // finance hints
    'New transaction' => 'New transaction',
    'Start typing the amount to add a new transaction' => 'Start typing the amount to add a new transaction',
    'Editing transaction' => 'Editing transaction',
    'Press Enter to move to the next field. You can also switch between the fields using keys to left/right. The date of the transaction can be selected in the top left corner of the page' => 'Press Enter to move to the next field. You can also switch between the fields using keys to left/right. The date of the transaction can be selected in the top left corner of the page',
    'Account and Budget' => 'Account and Budget',
    'Ctrl + up/down - the account. Alt + up/down - the budget. You can also select the account in the table by clicking on the appropriate line on accounts table' => 'Ctrl + up/down - the account. Alt + up/down - the budget. You can also select the account in the table by clicking on the appropriate line on accounts table',
    'Saving transaction' => 'Saving transaction',
    'Click in this area to preserve the current transaction' => 'Click in this area to preserve the current transaction',
);
