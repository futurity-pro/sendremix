<?php
Yii::import('bootstrap.widgets.TbDropdown');

class Subnav extends TbDropdown
{
	/**
	 * Initializes the widget.
	 */
	public function init()
	{
		if (isset($this->htmlOptions['class']))
			$this->htmlOptions['class'] .= ' submenu';
		else
			$this->htmlOptions['class'] = 'submenu';
	}
}